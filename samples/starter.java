//The most basic JavaFX program

package sample;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.DepthTest;
import javafx.stage.Stage;

import static javafx.scene.DepthTest.ENABLE;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Pane root = new Pane() {};
        //Depth test is required to make OpenGL render from back to front
        root.setDepthTest(ENABLE);
        //The second and third option are the width and height respectively
        //The fourth option enables Depth Test in the scene
        Scene scene = new Scene(root, 300, 275, true);

        //Set the title, scene, etc.
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}